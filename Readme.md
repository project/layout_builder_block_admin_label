# Layout Builder Block Admin Label

This module re-frames the block title field as an administrative label when
adding inline block to layout builder.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/layout_builder_block_admin_label).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/layout_builder_block_admin_label).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will alter Layout Builder inline block forms. To revert the
inline block forms to normal, disable the module and clear caches.

## Maintainers

- Jay Huskins - [jayhuskins](https://www.drupal.org/u/jayhuskins)
