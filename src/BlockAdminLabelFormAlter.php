<?php

namespace Drupal\layout_builder_block_admin_label;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\layout_builder\Form\AddBlockForm;
use Drupal\layout_builder\Form\UpdateBlockForm;
use Drupal\layout_builder\Plugin\Block\InlineBlock;

/**
 * Alters inline block forms to re-frame title field as an administrative label.
 */
class BlockAdminLabelFormAlter {

  use StringTranslationTrait;

  /**
   * Check if form is a valid layout builder inline block form and alter it.
   */
  public function alter($form, FormStateInterface $form_state) {
    $form_object = $form_state->getFormObject();

    // Only target Layout Builder update and add block forms.
    if ($form_object instanceof UpdateBlockForm || $form_object instanceof AddBlockForm) {
      $component = $form_object->getCurrentComponent();
      $plugin = $component->getPlugin();
      // Only modify form for inline blocks.
      if ($plugin instanceof InlineBlock) {
        // Get the default admin label from the inline block plugin label.
        $plugin_definition = $plugin->getPluginDefinition();
        $default_admin_label = $plugin_definition['admin_label'] ?? $plugin->getPluginId();
        return $this->doAlter($form, $default_admin_label);
      }
    }

    return $form;
  }

  /**
   * Make form alterations.
   */
  protected function doAlter($form, $default_admin_label = NULL) {
    // Re-label title to 'Administrative label' and clarify description.
    if (isset($form['settings']['label'])) {
      $form['settings']['label']['#title'] = $this->t('Administrative label');
      $form['settings']['label']['#description'] = $this->t(
        'This label will only be displayed on the layout edit page. It is useful 
        to distinguish multiple blocks of the same type when the content preview 
        is turned off.'
      );
      // Hide field.
      $form['settings']['admin_label']['#type'] = "hidden";
      // Set default value if no value has been set.
      if (!empty($default_admin_label) && !isset($form['settings']['label']['#default_value'])) {
        $form['settings']['label']['#default_value'] = $default_admin_label;
      }
    }
    // Hide display option and set it to false.
    if (isset($form['settings']['label_display'])) {
      $form['settings']['label_display']['#default_value'] = FALSE;
      $form['settings']['label_display']['#value'] = FALSE;
      $form['settings']['label_display']['#type'] = "hidden";
    }

    return $form;
  }

}
